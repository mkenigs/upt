"""Test cases for restraint_wrap/misc module."""
import os
import unittest

from restraint_wrap import misc


class TestRCMisc(unittest.TestCase):
    """Test cases for restraint_wrap/misc module."""

    def test_add_directory_suffix(self):
        """Ensure add_directory_suffix works."""
        output = misc.add_directory_suffix('tests', 1)
        self.assertEqual(output, os.path.abspath('tests') + '.done.01')
