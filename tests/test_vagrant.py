"""Test cases for vagrant provisioner module."""
import unittest
from unittest import mock

from plumbing.objects import Host
from provisioners.vagrant import Vagrant


class TestVagrant(unittest.TestCase):
    # pylint: disable=no-member
    """Test cases for vagrant module."""

    @mock.patch('provisioners.vagrant.Vagrant._provision')
    def test_provision(self, mock_provision):
        """Ensure provision calls _provision."""
        Vagrant().provision()
        mock_provision.asssert_called()

    @mock.patch('provisioners.vagrant.safe_popen')
    @mock.patch('provisioners.vagrant.LOGGER.error')
    def test_provision_host(self, log_error, mock_safe_popen):
        """Ensure provision_host works."""
        out = """id  name         provider state   directory
------------------------------------
aaaaaaa  1MT-RHEL-7 libvirt running /home/juser/b
fffffff  1MT-RHEL-8 libvirt running /home/juser/a"""

        mock_safe_popen.side_effect = iter(
            [(out, 'ERR', 0), ('', '', 0), ('HostName hostname', '', 0)])
        vagrant = Vagrant()
        host = Host({'misc': {'instance_id': 'fffffff'}})

        vagrant.provision_host(host)
        log_error.assert_called_with('ERR')

    @mock.patch('provisioners.vagrant.safe_popen')
    def test_provision_host_no_effect(self, mock_safe_popen):
        """Ensure provision_host has no effect when instance_id does not match."""
        mock_safe_popen.side_effect = ('', '', 0),
        vagrant = Vagrant()
        host = Host({'misc': {'instance_id': 'fffffff'}})

        vagrant.provision_host(host)
